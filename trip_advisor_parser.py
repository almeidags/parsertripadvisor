import os

def ProcessaArqPorEstado(caminhoLer, listaUF):
    for UF in listaUF:
        caminhoLerUF = caminhoLer + UF + "/"
        nomeArqOK = "resultados/TripAdvisorOk" + UF + ".txt"
        nomeArqErro = "resultados/TripAdvisorErro" + UF + ".txt"
        print("Processando {} - {}".format(UF, caminhoLerUF))
        ProcessaArqs(caminhoLerUF, nomeArqOK, nomeArqErro)
    

def ProcessaArqs(caminhoLer, nomeArqOK, nomeArqErro):
    files = []

    caminhos = []
    for cam in caminhoLer:
        caminhos.append(cam)
    while len(caminhos) > 0:
        cam = caminhos[0]
        for name in os.listdir(cam):
            if os.path.isfile(os.path.join(cam, name)):
                files.append(cam + name)
            else:
                caminhos.append(cam + name + "/")
        caminhos.pop(0)


    ratingsParciais = ['Custo-benefÃ­cio',
                        'LocalizaÃ§Ã£o',
                        'Qualidade do sono',
                        'Quartos',
                        'Limpeza',
                        'Atendimento',
                        'Outros']
    valorRatingsParciais = []
    for _ in ratingsParciais:
        valorRatingsParciais.append("0")

    termosComplemento = ["sozinho(a)",
                         "amigos",
                         "negÃ³cios",
                         "famÃ­lia",
                         "casal",
                         "trabalho",
                         "a dois",
                         "sozinho",
                         "sozinha"]

    arqSalvarOk = open(nomeArqOK, "a")
    arqSalvarEr = open(nomeArqErro, "a")

    exibirArquivo = True
    exibirNomeHotel = False
    exibirLocalHotel = True
    exibirTituloAvaliacao = False
    exibirRatingGeral = True
    exibirDataAvaliacao = True
    exibirIDAvaliacao = True
    exibirTextoAvaliacao = False
    exibirComplAvaliacao = True
    exibirRatingsParciais = True

    separador = "|"
    
    contOk = 0
    contEr = 0
    for nomeArqLer in files:

        if (contOk + contEr) % 100 == 0:
            arqSalvarOk.close()
            arqSalvarEr.close()
            arqSalvarOk = open(nomeArqOK, "a")
            arqSalvarEr = open(nomeArqErro, "a")

        with open(nomeArqLer, mode='r', encoding='utf-8') as arq:

            texto = arq.read()
            
            NomeHotel = ""
            LocalHotel = ""
            
            # Informacoes do Hotel
            ind = texto.find('<a class="HEADING"')
            if ind > -1:
                texto = texto[ind:]
                ind = texto.find('>')
                ind2 = texto.find('</a>')
                temp = texto[ind+1 : ind2].strip()
                # Salva nome do Hotel
                texto = texto[ind2+4:]

                NomeHotel = temp

                ind = texto.find('<span class="locality">')
                texto = texto[ind + len('<span class="locality">'):]
                ind = texto.find('</span>')
                temp = texto[:ind]
                texto = texto[ind + len('</span>') + 1:]
                # Salva localizacao do Hotel
                
                LocalHotel = temp.replace(",", "").strip()
            
            ind = texto.find("<!--trkR")
            if ind == -1:
                formato = 1
            else:
                formato = 2

            while True:
                # Loop para buscar avaliacoes.
                # Condicao de parada na verificacao do Usuario ou do Titulo da Avaliacao  

                TituloAvaliacao = ""
                RatingGeral = ""
                DataAvaliacao = ""
                IDAvaliacao = ""
                TextoAvaliacao = ""
                ComplAvaliacao = ""
                RatingParcial = ""


                if formato == 1:

                    # Informacoes da Avaliacao
                    ind = texto.find('class="quote">')
                    if ind == -1:
                        break

                    # Salva titulo da Avaliacao
                    texto = texto[ind:]
                    ind = texto.find('>')
                    ind2 = texto.find('</div>')
                    temp = texto[ind+1 : ind2].strip()
                    texto = texto[ind2 + len('</div>'):]
    
                    if temp.find("class='noQuotes'>") > -1:
                        # Nao eh a primeira avaliacao da pagina.
                        ind = temp.find("class='noQuotes'>") + len("class='noQuotes'>")
                        ind2 = temp.find("</span>")
                        temp = temp[ind:ind2]
                    
                    TituloAvaliacao = temp.replace(",", "")
                    
                    # Salva avaliacao numerica geral
                    ind = texto.find('class="ui_bubble_rating')
                    texto = texto[ind + len('class="ui_bubble_rating') + 1:]
                    ind2 = texto.find('"')
                    temp = texto[:ind2-1].replace("bubble_", "")
                    
                    RatingGeral = temp 
    
                    # Salva data da avaliacao
                    texto = texto[ind2+1:]
                    ind = texto.find('class="ratingDate')
                    texto = texto[ind + len('class="ratingDate'):]
                    ind = texto.find('>')
                    texto2 = texto[:ind]
                    texto = texto[ind + len('>'):]
                    ind = texto.find('<')
                    temp = texto[:ind].strip()
                    texto = texto[ind + len('</span>'):]
    
                    if texto2.find("relativeDate") > -1: # Se tiver data relativa
                        ind = texto2.find("title='")
                        texto2 = texto2[ind + len("title='"):]
                        ind = texto2.find("'")
                        temp = texto2[:ind]
                    
                    DataAvaliacao = temp.replace("Avaliou em ", "").replace(" de ", "-")
    
                    # Salva ID da Avaliacao
                    ind = texto.find('<div class="entry">')
                    texto = texto[ind + len('<div class="entry">'):]
                    ind = texto.find('id="review_')
                    texto = texto[ind + len('id="review_'):]
                    ind = texto.find('"')
                    temp = texto[:ind]
                    texto = texto[ind + len('">'):]
    
                    IDAvaliacao = temp
                    
                    # Salva texto da Avaliacao
                    ind = texto.find('</p>')
                    temp = texto[:ind].strip()
                    
                    TextoAvaliacao = temp.replace(",", "")
                    
                    texto = texto[ind + len('</p>'):]
    
                    # Salva complemento da Avaliacao
                    ind = texto.find('class="recommend-titleInline')
                    texto = texto[ind:]
                    ind = texto.find('>')
                    ind2 = texto.find('</span>')
                    temp = texto[ind+1 : ind2].strip()
                    texto = texto[ind2 + len('</span>'):]
                    
                    for termo in termosComplemento:
                        if temp.find(termo) > -1:
                            ComplAvaliacao = termo
                            
                    ComplAvaliacao = temp.replace(",", "")
                    
                    ind = texto.find('<div class="note">')
                    texto2 = texto[:ind] # Separo em texto2 os rating parciais
                    texto = texto[ind + len('<div class="note">'):]
    
                    RatingParcial = valorRatingsParciais
                    if exibirRatingsParciais:
                        listatemp = valorRatingsParciais.copy()
                        ind = texto2.find('class="ui_bubble_rating')
                        while ind > -1:
                            # Salva avaliacao numerica de cada item (Bubble + Textual)
                            texto2 = texto2[ind + len('class="ui_bubble_rating') + 1:]
                            ind2 = texto2.find('"')
                            temp = texto2[:ind2].replace("bubble_", "")
                            texto2 = texto2[ind2+1:]
        
                            ind = texto2.find('class="recommend-description">')
                            texto2 = texto2[ind + len('class="recommend-description">'):]
                            ind2 = texto2.find('<')
                            itemAvaliado = texto2[:ind2]
                            texto2 = texto2[ind2+1:]
                            
                            #Identifica o item avaliado, e preenche o valor
                            try:
                                ind = ratingsParciais.index(itemAvaliado)
                            except:
                                ind = 6
                            listatemp[ind] = temp
                            
                            ind = texto2.find('class="ui_bubble_rating')
                    
                        temp = ""
                        for valor in listatemp:
                            temp += str(valor) + separador
                        temp = temp[:-len(separador)] # Retiro ultimo separador
    
                        RatingParcial = temp

                if formato == 2:

                    # Informacoes da Avaliacao
                    ind = texto.find("class='noQuotes'>")
                    if ind == -1:
                        break # Nao existem mais avaliacoes.

                    # Salva ID da Avaliacao
                    ind = texto.find('id="review_')
                    texto = texto[ind + len('id="review_'):]
                    ind = texto.find('"')
                    temp = texto[:ind]
                    texto = texto[ind + len('">'):]
    
                    IDAvaliacao = temp

                    # Salva avaliacao numerica geral
                    ind = texto.find('class="ui_bubble_rating')
                    texto = texto[ind + len('class="ui_bubble_rating') + 1:]
                    ind2 = texto.find('"')
                    temp = texto[:ind2-1].replace("bubble_", "")
                    
                    RatingGeral = temp 
    
                    # Salva data da avaliacao
                    texto = texto[ind2+1:]
                    ind = texto.find('class="ratingDate')
                    texto = texto[ind + len('class="ratingDate'):]
                    ind = texto.find('>')
                    texto2 = texto[:ind]
                    texto = texto[ind + len('>'):]
                    ind = texto.find('<')
                    temp = texto[:ind].strip()
                    texto = texto[ind + len('</span>'):]
    
                    if texto2.find("relativeDate") > -1: # Se tiver data relativa
                        ind = texto2.find("title='")
                        texto2 = texto2[ind + len("title='"):]
                        ind = texto2.find("'")
                        temp = texto2[:ind]
                    
                    DataAvaliacao = temp.replace("Avaliou em ", "").replace(" de ", "-")

                    # Salva titulo da Avaliacao
                    texto = texto[ind:]
                    ind = texto.find('>')
                    ind2 = texto.find('</div>')
                    temp = texto[ind+1 : ind2].strip()
                    texto = texto[ind2 + len('</div>'):]
    
                    if temp.find("class='noQuotes'>") > -1:
                        # Nao eh a primeira avaliacao da pagina.
                        ind = temp.find("class='noQuotes'>") + len("class='noQuotes'>")
                        ind2 = temp.find("</span>")
                        temp = temp[ind:ind2]
                    
                    TituloAvaliacao = temp.replace(",", "")
                    
                    # Salva texto da Avaliacao
                    ind = texto.find('<p class="partial_entry">')
                    texto = texto[ind:]                    
                    ind = texto.find('</p>')
                    temp = texto[:ind].strip()
                    
                    TextoAvaliacao = temp.replace(",", "")
                    
                    texto = texto[ind + len('</p>'):]
    
                    # Salva complemento da Avaliacao
                    ind = texto.find('class="recommend-titleInline')
                    texto = texto[ind:]
                    ind = texto.find('<span class="stayed"')
                    texto = texto[ind:]
                    ind = texto.find('</span>')
                    texto = texto[ind + len('</span>'):]
                    ind2 = texto.find('</span>')
                    temp = texto[:ind2].strip()
                    texto = texto[ind2 + len('</span>'):]
                    for termo in termosComplemento:
                        if temp.find(termo) > -1:
                            ComplAvaliacao = termo
                            
                    ComplAvaliacao = temp.replace(",", "")
                    
                    ind = texto.find('<!--etk-->')
                    texto2 = texto[:ind] # Separo em texto2 os rating parciais
                    texto = texto[ind + len('<!--etk-->'):]
    
                    RatingParcial = valorRatingsParciais
                    if exibirRatingsParciais:
                        listatemp = valorRatingsParciais.copy()
                        ind = texto2.find('class="ui_bubble_rating')
                        while ind > -1:
                            # Salva avaliacao numerica de cada item (Bubble + Textual)
                            texto2 = texto2[ind + len('class="ui_bubble_rating') + 1:]
                            ind2 = texto2.find('"')
                            temp = texto2[:ind2].replace("bubble_", "")
                            texto2 = texto2[ind2+1:]
        
                            ind = texto2.find('class="recommend-description">')
                            texto2 = texto2[ind + len('class="recommend-description">'):]
                            ind2 = texto2.find('<')
                            itemAvaliado = texto2[:ind2]
                            texto2 = texto2[ind2+1:]
                            
                            #Identifica o item avaliado, e preenche o valor
                            try:
                                ind = ratingsParciais.index(itemAvaliado)
                            except:
                                ind = 6
                            listatemp[ind] = temp
                            
                            ind = texto2.find('class="ui_bubble_rating')
                    
                        temp = ""
                        for valor in listatemp:
                            temp += str(valor) + separador
                        temp = temp[:-len(separador)] # Retiro ultimo separador
    
                        RatingParcial = temp

                # Para cada avaliacao, exibo uma linha
                linha = ""
                erro = 0
                if exibirArquivo:
                    linha += nomeArqLer + separador
                if exibirNomeHotel:
                    if NomeHotel == "":
                        erro += 1
                    linha += NomeHotel + separador
                if exibirLocalHotel:
                    if LocalHotel == "":
                        erro += 1
                    linha += LocalHotel + separador
                if exibirIDAvaliacao:
                    if IDAvaliacao == "":
                        erro += 1
                    linha += IDAvaliacao + separador
                if exibirTituloAvaliacao:
                    if TituloAvaliacao == "":
                        erro += 1
                    linha += TituloAvaliacao + separador
                if exibirRatingGeral:
                    if RatingGeral == "":
                        erro += 1
                    linha += RatingGeral + separador
                if exibirTextoAvaliacao:
                    if TextoAvaliacao == "":
                        erro += 1
                    linha += TextoAvaliacao + separador
                if exibirComplAvaliacao:
                    if ComplAvaliacao == "":
                        erro += 1
                    linha += ComplAvaliacao + separador
                if exibirDataAvaliacao:
                    if DataAvaliacao == "":
                        erro += 1
                    linha += DataAvaliacao + separador
                if exibirRatingsParciais:
                    if RatingParcial == "":
                        erro += 1
                    linha += RatingParcial + separador
                
                if erro > 0:
                    arqSalvarEr.write("{}\n".format(linha))
                    contEr += 1
                else:
                    linha = linha[:-len(separador)] # Retiro ultimo separador
                    arqSalvarOk.write("{}\n".format(linha))
                    contOk += 1

                
    print("{} Avaliacoes completas\n".format(contOk))
    print("{} Avaliacoes incompletas\n".format(contEr))
    arqSalvarOk.write("{} Avaliacoes completas\n".format(contOk))
    arqSalvarEr.write("{} Avaliacoes incompletas\n".format(contEr))
    arqSalvarOk.close()
    arqSalvarEr.close()

def RetiraDuplicados(listaArquivosComRep, listaArquivosSemRep):
    lista = []
    for indArq in range(len(listaArquivosSemRep)):
        arqNovo = open(listaArquivosSemRep[indArq], "a")
        dupl = 0
        ok = 0
        with open(listaArquivosComRep[indArq], 'r') as arq:
            for linha in arq:
                ind = linha.find("|") # Nome do arquivo
                temp = linha[ind+1:]
                ind = temp.find("|") # Localizacao
                temp = temp[ind+1:]
                ind = temp.find("|") # Informacoes apos o ID
                temp = temp[:ind]
                try:
                    pos = lista.index(temp)
                    dupl += 1
                except Exception:
                    lista.append(temp)
                    arqNovo.write(linha)
                    ok += 1
        arqNovo.write("{} linhas ok\n{} linhas duplicadas\n".format(ok, dupl))
        arqNovo.close()
        print("{} linhas ok\n{} linhas duplicadas\n".format(ok, dupl))
        
        
if __name__ == '__main__':

    '''
    # Para avaliacoes separadas por UF
    listaUF = ['AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO',
               'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR',
               'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO']
    
    caminhoArqsUF = "resultados/Dados/com.br/"
    ProcessaArqPorEstado(caminhoArqsUF, listaUF)

    for UF in listaUF:
        listaArquivosComRep.append("resultados/TripAdvisorOk" + UF + ".txt")
        listaArquivosSemRep.append("resultados/TripAdvisorOkSemDuplicados" + UF + ".txt")

    RetiraDuplicados(listaArquivosComRep, listaArquivosSemRep)
    '''

    # Para avaliacoes agrupadas em uma unica pasta
    nomeArqOK = "resultados/TripAdvisorOk.txt"
    nomeArqErro = "resultados/TripAdvisorErro.txt"
    caminhoArqs = []
    caminhoArqs.append("resultados/Dados/com.br/")
    ProcessaArqs(caminhoArqs, nomeArqOK, nomeArqErro)
    
    listaArquivosComRep = []
    listaArquivosComRep.append("resultados/TripAdvisorOk.txt")
    listaArquivosSemRep = []
    listaArquivosSemRep.append("resultados/TripAdvisorOkSemDuplicados.txt")
    
    RetiraDuplicados(listaArquivosComRep, listaArquivosSemRep)
    